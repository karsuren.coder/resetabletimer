package com.karsuren.learn.resetabletimer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.karsuren.learn.resetabletimer.timer.ResetableTimer;

public class MainActivity extends AppCompatActivity {

    ResetableTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timer = ResetableTimer.startNewTimer(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this.getApplicationContext(), "Timout", Toast.LENGTH_SHORT).show();
            }
        }, 5000L);

        Button resetButton = (Button) findViewById(R.id.reset_btn);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.reset();
            }
        });
    }
}