package com.karsuren.learn.resetabletimer.timer;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;

import androidx.annotation.NonNull;

public class ResetableTimer {
    public static final String THEAD_NAME = ResetableTimer.class.getCanonicalName()+"-timer-thread";
    public static final int TIMEOUT_MESSAGE_ID = 1;

    private HandlerThread mHandlerThread;
    private Handler mHandler;
    private long mTimeoutMilli;

    private ResetableTimer(final Runnable callback, final long timeoutMilli) {
        mHandlerThread = new HandlerThread(THEAD_NAME, Process.THREAD_PRIORITY_BACKGROUND);
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper()){
            @Override
            public void handleMessage(@NonNull Message msg) {
                callback.run();
            }
        };
        mTimeoutMilli = timeoutMilli;
        mHandler.sendMessageDelayed(mHandler.obtainMessage(TIMEOUT_MESSAGE_ID), mTimeoutMilli);
    }

    public static ResetableTimer startNewTimer(final Runnable callback, final long timeoutMilli) {
        return new ResetableTimer(callback, timeoutMilli);
    }

    public void reset() {
        mHandler.removeMessages(TIMEOUT_MESSAGE_ID);
        mHandler.sendEmptyMessageDelayed(TIMEOUT_MESSAGE_ID, mTimeoutMilli);
    }

    public void stop() {
        mHandlerThread.quit();
    }
}
